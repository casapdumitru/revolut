﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Revolut.Models;

namespace Revolut.DtoModels
{
    public class CreateUserDTO
    {
        [Required]
        [StringLength(13)]
        [RegularExpression("^[0-1]+[0-9]*$")]
		public string CNP { get; set; }
        [Required]
        [MinLength(3), MaxLength(30)]
        public string FirstName { get; set; }
        [Required]
        [MinLength(3), MaxLength(30)]
        public string LastName { get; set; }
        [Required]
        [MinLength(8), MaxLength(30)]
        public string UserName { get; set; }
        [Required]
        [MinLength(8), MaxLength(30)]
        public string Password { get; set; }
        [MaxLength(30)]
        public string Address { get; set; }

	    /*public virtual IList<BankAccount> BankAccounts { get; set; }
	    public virtual IList<Group> Groups { get; set; }*/
	}
}
