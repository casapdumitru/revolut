﻿using System;

namespace Revolut.Models
{
    public class BankAccount
    {
        public string IbanNumber { get; set; }
        public double Budget { get; set; }
        public DateTime Created { get; set; }

        public User Owner { get; set; }
    }
}
