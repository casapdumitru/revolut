﻿using System.Collections.Generic;

namespace Revolut.Models
{
    public class User
    {
        public string CNP { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public string Address { get; set; }

        public virtual IList<BankAccount> BankAccounts { get; set; }
        public virtual IList<Group> Groups { get; set; }
    }
}
