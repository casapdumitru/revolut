Create Database RevolutDB

Create Table Users(
Cnp varchar(13) Not Null Primary Key,
FirstName varchar(30) Not Null,
LastName varchar(30) Not Null,
UserName varchar(30) Not Null,
Password varchar(30) Not Null,
Address varchar(50) )

Create Table BankAccounts(
IbanNumber varchar(20) Not Null Primary Key,
Budget float Not null,
Created DateTime Not Null,
OwnerCnp varchar(13) Foreign Key References Users(Cnp))

Create Table Groups(
Id int Identity(1,1) Not Null Primary Key,
Name varchar(30) Not Null,
Description varchar(100),
OwnerCnp varchar(13) Foreign Key References Users(Cnp))

Create Table UsersGroups(
UserCnp varchar(13) Foreign Key References Users(Cnp),
GroupId int Foreign Key References Groups(Id),
Primary Key (UserCnp, GroupId))


Insert Into Users
Values('1234567891234', 'Alex', 'Popa', 'AlexPopa','123456', 'Romania, Cluj')

Insert Into Users
Values('1321567891234', 'Andrei', 'Hancu', 'AndreiHancu','956754', 'Romania, Cluj, Dorobantilor')

Insert Into Users
Values('2234598761234', 'Gabriela', 'Matei', 'Matei','parola', 'Romania, Cluj')

Insert Into Users
Values('1234567891214', 'Tudor', 'Lung', 'TudorLung','no_chances', 'Romania, Cluj')

Insert Into Users
Values('1234567891233', 'Dumitru', 'Casap', 'Casap','12456423', 'Romania, Cluj')


Insert Into BankAccounts
Values('421321421321', 100.4, '2018-11-07', '2234598761234')

Insert Into BankAccounts
Values('7324523623', 1200.4, '2017-04-21', '1234567891234')

Insert Into BankAccounts
Values('7843236', 0, '2018-07-13', '1234567891234')


Insert Into Groups(Name, Description, OwnerCnp)
Values('Group1', null, '1234567891234')

Insert Into UsersGroups
Values('1234567891234', 1)

Insert Into UsersGroups
Values('2234598761234', 1)




