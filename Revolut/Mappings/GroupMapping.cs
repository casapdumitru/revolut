﻿using Revolut.DtoModels;
using Revolut.Interfaces.Mapping;
using Revolut.Models;
using Revolut.ModelsDTO.Group;
using System.Collections.Generic;
using System.Linq;

namespace Revolut.Mapping
{
    public class GroupMapping : IGroupMapping
    {
        //private IUserMapping _userMapping;

        //public GroupMapping(IUserMapping userMapping)
        //{
        //    _userMapping = userMapping;
        //}

        public GroupDTO GetGroupDTO(Group group)
        {
            var groupDTO = new GroupDTO()
            {
                Id = group.Id,
                Name = group.Name,
                Description = group.Description
            };

            //if (group.Owner != null)
            //    groupDTO.Owner = _userMapping.GetUserDTO(group.Owner);

            //if (group.Members.Any())
            //{
            //    groupDTO.Members = new List<UserDTO>();

            //    foreach (var member in group.Members)
            //    {
            //        groupDTO.Members.Add(_userMapping.GetUserDTO(member));
            //    }
            //}

            return groupDTO;
        }

        public Group GetModelFromGroupDTO(GroupDTO groupDTO)
        {
            var group = new Group()
            {
                Id = groupDTO.Id,
                Name = groupDTO.Name,
                Description = groupDTO.Description
            };

            //if (groupDTO.Owner != null)
            //    group.Owner = _userMapping.GetModelFromUserDTO(groupDTO.Owner);

            //if (groupDTO.Members.Any())
            //{
            //    group.Members = new List<User>();

            //    foreach (var member in groupDTO.Members)
            //    {
            //        group.Members.Add(_userMapping.GetModelFromUserDTO(member));
            //    }
            //}

            return group;
        }
    }
}
