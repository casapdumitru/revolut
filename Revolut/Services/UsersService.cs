﻿using System;
using Revolut.DtoModels;
using Revolut.Mapping;
using Revolut.Models;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using Microsoft.EntityFrameworkCore;
using Revolut.Interfaces.Repositories;
using Revolut.ModelsDTO.BankAccount;
using Revolut.ModelsDTO.Group;

namespace Revolut.Services
{
	public class UsersService : IUserService
    {
	    private IUserRepository _userRepository;
	    private IMapper _mapper;

        public UsersService(IUserRepository userRepository, IMapper mapper)
        {
	        _userRepository = userRepository;
	        _mapper = mapper;
        }

        public IList<UserDTO> GetUsers()
        {
	        
	        var users = _userRepository.GetAll();

	        var usersDTO = _mapper.Map<List<UserDTO>>(users);

	        return usersDTO;
        }

	    public UserDTO GetUser(string cnp)
	    {
		    var user = _userRepository.GetByCnp(cnp);

		    if (user == null)
			    throw new NullReferenceException("User with this cnp is not found");

		    return _mapper.Map<UserDTO>(user);
	    }

	    public UserDTO GetFullUser(string cnp)
	    {
		    var user = _userRepository.GetFullUserByCnp(cnp);

		    if (user == null)
			    throw new NullReferenceException("User with this cnp is not found");

		    var userDTO =_mapper.Map<UserDTO>(user);s
		    return userDTO;
	    }

		public IList<UserDTO> GetUserByName(string firstName, string lastName)
	    {
		    return null;
	    }

		public UserDTO AddUser(CreateUserDTO userDTO)
		{
			var u = _userRepository.GetByCnp(userDTO.CNP);

			if (u != null)
				throw new NullReferenceException("User with this cnp already exist");

			var user = _mapper.Map<User>(userDTO);
			user = _userRepository.Add(user);

			return _mapper.Map<UserDTO>(user);
		}

	    public UserDTO UpdateUser(CreateUserDTO userDTO)
	    {
		    var us = _userRepository.GetByCnp(userDTO.CNP);

		    if (us == null)
			    return null;

		    var user = _mapper.Map<User>(userDTO);
			_userRepository.Update(user);

		    return _mapper.Map<UserDTO>(user);
	    }

		public UserDTO DeleteUser(string cnp)
		{
			var user = _userRepository.GetByCnp(cnp);

            if (user == null)
                throw new NullReferenceException($"User with cnp = {cnp} is not found");

            _userRepository.Delete(cnp);

	        return _mapper.Map<UserDTO>(user);
        }
    }
}
