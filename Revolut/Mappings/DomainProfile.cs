﻿
using AutoMapper;
using Revolut.DtoModels;
using Revolut.Models;

namespace Revolut.Mappings
{

	public class DomainProfile : Profile
	{
		public DomainProfile()
		{
			CreateMap<CreateUserDTO, User>().ForMember(u => u.BankAccounts, opt => opt.Ignore()).ForMember(u => u.Groups, opt => opt.Ignore());
		}
	}
}
