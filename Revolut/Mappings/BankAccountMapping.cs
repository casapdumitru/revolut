﻿using Revolut.Interfaces.Mapping;
using Revolut.Models;
using Revolut.ModelsDTO.BankAccount;

namespace Revolut.Mapping
{
    public class BankAccountMapping : IBankAccountMapping
    {
        //private IUserMapping _userMapping;

        //public BankAccountMapping(IUserMapping userMapping)
        //{
        //    _userMapping = userMapping;
        //}

        public BankAccountDTO GetBankAccountDTO(BankAccount bankAccount)
        {
            var bankAccountDTO = new BankAccountDTO()
            {
                IbanNumber = bankAccount.IbanNumber,
                Created = bankAccount.Created
            };

            //if (bankAccount.Owner != null)
            //    bankAccountDTO.Owner = _userMapping.GetUserDTO(bankAccount.Owner);

            return bankAccountDTO;
        }

        public BankAccount GetModelFromBankAccountDTO(BankAccountDTO bankAccountDTO)
        {
            var bankAccount = new BankAccount()
            {
                IbanNumber = bankAccountDTO.IbanNumber,
                Created = bankAccountDTO.Created
            };

            //if (bankAccount.Owner != null)
            //    bankAccount.Owner = _userMapping.GetModelFromUserDTO(bankAccountDTO.Owner);

            return bankAccount;
        }

        public PersonalBankAccountDTO GetPersonaBankAccountDTO(BankAccount bankAccount)
        {
            var personalBankAccountDTO = new PersonalBankAccountDTO()
            {
                IbanNumber = bankAccount.IbanNumber,
                Created = bankAccount.Created,
                Budget = bankAccount.Budget
            };

            //if (bankAccount.Owner != null)
            //    personalBankAccountDTO.Owner = _userMapping.GetUserDTO(bankAccount.Owner);

            return personalBankAccountDTO;
        }

        public BankAccount GetModelFromPersonalBankAccountDTO(PersonalBankAccountDTO personalBankAccountDTO)
        {
            var personalBankAccount = new BankAccount()
            {
                IbanNumber = personalBankAccountDTO.IbanNumber,
                Created = personalBankAccountDTO.Created,
                Budget = personalBankAccountDTO.Budget
            };

            //if (personalBankAccount.Owner != null)
            //    personalBankAccount.Owner = _userMapping.GetModelFromUserDTO(personalBankAccountDTO.Owner);

            return personalBankAccount;
        }
    }
}
