﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Revolut.ModelsDTO.BankAccount;
using Revolut.ModelsDTO.Group;

namespace Revolut.DtoModels
{
    public class UserDTO
    {
        [Required]
        [StringLength(13)]
		[RegularExpression("^[0-1]*[0-9]$")]
        public string CNP { get; set; }
        [Required]
        [MaxLength(30)]
        public string FirstName { get; set; }
        [Required]
        [MaxLength(30)]
        public string LastName { get; set; }
        public string Address { get; set; }

        public IList<BankAccountDTO> BankAccounts { get; set; }
        public IList<GroupDTO> Groups { get; set; }
    }
}
