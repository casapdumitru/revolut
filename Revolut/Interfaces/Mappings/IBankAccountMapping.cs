﻿using Revolut.Models;
using Revolut.ModelsDTO.BankAccount;

namespace Revolut.Interfaces.Mapping
{
    public interface IBankAccountMapping
    {
        BankAccountDTO GetBankAccountDTO(BankAccount bankAccount);
        BankAccount GetModelFromBankAccountDTO(BankAccountDTO bankAccountDTO);

        PersonalBankAccountDTO GetPersonaBankAccountDTO(BankAccount bankAccount);
        BankAccount GetModelFromPersonalBankAccountDTO(PersonalBankAccountDTO personalBankAccountDTO);
    }
}