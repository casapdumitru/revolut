﻿using Revolut.DtoModels;
using System;
using System.ComponentModel.DataAnnotations;

namespace Revolut.ModelsDTO.BankAccount
{
    public class BankAccountDTO
    {
        [Required]
        public string IbanNumber { get; set; }
        public DateTime Created { get; set; }

        [Required]
        public UserDTO Owner { get; set; }
    }
}
