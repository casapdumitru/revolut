﻿using Revolut.DtoModels;
using System;
using System.ComponentModel.DataAnnotations;

namespace Revolut.ModelsDTO.BankAccount
{
    public class PersonalBankAccountDTO
    {
        [Required]
        public string IbanNumber { get; set; }
        public double Budget { get; set; }
        public DateTime Created { get; set; }

        [Required]
        public UserDTO Owner { get; set; }

    }
}
