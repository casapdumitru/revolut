﻿using System.Collections.Generic;
using Revolut.DtoModels;

namespace Revolut.Services
{
    public interface IUserService
    {
        IList<UserDTO> GetUsers();

        UserDTO GetUser(string cnp);
	    UserDTO GetFullUser(string cnp);
		UserDTO AddUser(CreateUserDTO user);

        UserDTO UpdateUser(CreateUserDTO user);

        UserDTO DeleteUser(string cnp);

        IList<UserDTO> GetUserByName(string firstName, string lastName);
    }
}
