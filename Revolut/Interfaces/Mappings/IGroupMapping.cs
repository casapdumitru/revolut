﻿using Revolut.Models;
using Revolut.ModelsDTO.Group;

namespace Revolut.Interfaces.Mapping
{
    public interface IGroupMapping
    {
        GroupDTO GetGroupDTO(Group group);
        Group GetModelFromGroupDTO(GroupDTO groupDTO);
    }
}