﻿using Revolut.DtoModels;
using Revolut.Interfaces.Mapping;
using Revolut.Models;
using Revolut.ModelsDTO.BankAccount;
using Revolut.ModelsDTO.Group;
using System.Collections.Generic;
using System.Linq;

namespace Revolut.Mapping
{
    public class UserMapping : IUserMapping
    {
        private IBankAccountMapping _bankAccountMapping;
        private IGroupMapping _groupMapping;

        public UserMapping(IBankAccountMapping bankAccountMapping, IGroupMapping groupMapping)
        {
            _bankAccountMapping = bankAccountMapping;
            _groupMapping = groupMapping;
        }

        public UserDTO GetUserDTO(User user)
        {
            var userDTO = new UserDTO()
            {
                CNP = user.CNP,
                FirstName = user.FirstName,
                LastName = user.LastName,
                Address = user.Address
            };

            if (user.BankAccounts != null && user.BankAccounts.Any())
            {
                userDTO.BankAccounts = new List<BankAccountDTO>();

                foreach (var bankAccount in user.BankAccounts)
                {
                    userDTO.BankAccounts.Add(_bankAccountMapping.GetBankAccountDTO(bankAccount));
                }
            }

            if (user.Groups != null && user.Groups.Any())
            {
                userDTO.Groups = new List<GroupDTO>();

                foreach (var group in user.Groups)
                {
                    userDTO.Groups.Add(_groupMapping.GetGroupDTO(group));
                }
            }

            return userDTO;
        }

        public User GetModelFromUserDTO(UserDTO userDTO)
        {
            var user = new User()
            {
                CNP = userDTO.CNP,
                FirstName = userDTO.FirstName,
                LastName = userDTO.LastName,
                Address = userDTO.Address
            };

            if (user.BankAccounts != null && user.BankAccounts.Any())
            {
                user.BankAccounts = new List<BankAccount>();

                foreach (var bankAccountDTO in userDTO.BankAccounts)
                {
                    user.BankAccounts.Add(_bankAccountMapping.GetModelFromBankAccountDTO(bankAccountDTO));
                }
            }

            if (user.Groups != null && user.Groups.Any())
            {
                user.Groups = new List<Group>();

                foreach (var groupDTO in userDTO.Groups)
                {
                    user.Groups.Add(_groupMapping.GetModelFromGroupDTO(groupDTO));
                }
            }

            return user;
        }

        public CreateUserDTO GetCreateUserDTO(User user)
        {
            var dtoUser = new CreateUserDTO()
            {
                CNP = user.CNP,
                FirstName = user.FirstName,
                LastName = user.LastName,
                Address = user.Address,
                UserName = user.UserName,
                Password = user.Password
            };

            return dtoUser;
        }

        public User GetModelFromCreateUserDTO(CreateUserDTO userDTO)
        {
            var user = new User()
            {
                CNP = userDTO.CNP,
                FirstName = userDTO.FirstName,
                LastName = userDTO.LastName,
                Address = userDTO.Address,
                UserName = userDTO.UserName,
                Password = userDTO.Password
            };

            return user;
        }
    }
}
